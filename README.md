# setup ifml.io deveolopment environment

`git clone git@gitlab.com:metaaid/ifml-io/ifml-js.git`
`git clone git@gitlab.com:metaaid/ifml-io/moddle-xmi.git`
`git clone git@gitlab.com:metaaid/ifml-io/moddle-id.git`
`git clone git@gitlab.com:metaaid/ifml-io/ifml-moddle.git`
`git clone git@gitlab.com:metaaid/ifml-io/ifml-font.git`

# setup yarn

Node.js >=16.10

`corepack enable` (with admin rights)

`yarn plugin import workspace-tools`
`yarn install`
`yarn distro`

# in yarn dev
`yarn plugin import workspace-tools`